# Flask
from flask import request, render_template, make_response
from flask_assets import Environment, Bundle

# Helper
from .flask_helper import create_app
from .helper.database import Database
from .helper.views import Views
from xmltodict import parse
from xml.parsers.expat import ExpatError

from uuid import uuid4

app = create_app()
views = Views(app.config)


# Index
@app.route('/', methods=['GET'])
def index():
    """ Index
    ---
    get:
        summary: Shows a landing page
        tags:
            - Index
        responses:
            200:
                description: Default index page
    """
    assets = Environment(app)
    less = Bundle('css/normalize.css', 'css/layout.css', 'css/common.css', filters='cssmin', output='screen.css')
    assets.register('css_all', less)
    return render_template('landing.html')


@app.route('/mail/config-v1.1.xml', methods=['GET'])
def autoconfiguration():
    """ Thunderbird Autoconfiguration
    ---
    get:
        summary: Shows the thunderbird Autoconfiguration XML
        tags:
            - Autoconfiguration
        responses:
            200:
                description: Autoconfiguration XML
    """
    address = request.args.get('emailaddress', 'dummy@example.com')
    if '@' not in address:
        return 'No email address in `emailaddress` parameter.', 400
    database = Database(app.config, app.logger)
    username = database.get_username(address, '%EMAILADDRESS%')
    display_name = '%EMAILADDRESS%' if address == 'dummy@example.com' else database.get_displayname(address, username)

    response = make_response(views.render_view('config-v1.1.xml.j2', address, username, display_name))
    response.headers["Content-Type"] = "application/xml"
    return response


@app.route('/autodiscover/autodiscover.xml', methods=['POST'])
@app.route('/Autodiscover/Autodiscover.xml', methods=['POST'])
def autodiscover():
    """ Outlook Autoconfiguration
    ---
    post:
        summary: Shows the Outlook Autodiscover XML
        tags:
            - Autodiscover
        responses:
            200:
                description: Autodiscover XML
    """
    xml_data = request.data.decode('UTF-8')
    # dump request body to log for debugging
    app.logger.debug('Request body: ' + request.get_data(as_text=True))
    try:
        address = parse(xml_data)['Autodiscover']['Request']['EMailAddress']
    except (ExpatError, TypeError):
        return 'request body could not be parsed as XML', 400
    except KeyError as error:
        return 'Missing field in XML request body: {}'.format(error), 400
    if address is None:
        return '`EMailAddress` field is empty.', 400
    if '@' not in address:
        return 'No email address in request.', 400

    database = Database(app.config, app.logger)
    username = database.get_username(address)
    display_name = database.get_displayname(address, username)

    response = make_response(views.render_view('Autodiscover.xml.j2', address, username, display_name))
    response.headers["Content-Type"] = "application/xml"
    return response


@app.route('/email.mobileconfig', methods=['GET'])
def mobileconf():
    """ Mobile Autoconfiguration
    ---
    get:
        summary: Shows the Mobile Autoconfiguration XML
        tags:
            - Autoconfiguration
        responses:
            200:
                description: Autoconfiguration XML
    """
    address = request.args.get('email')
    if address is None:
        return 'No `email` parameter provided.', 400
    if '@' not in address:
        return 'No address provided in `email` parameter.', 400
    database = Database(app.config, app.logger)
    username = database.get_username(address)
    display_name = database.get_displayname(address, username)
    uuid = str(uuid4())

    response = make_response(views.render_view('email.mobileconfig.j2', address, username, display_name, uuid))
    response.headers["Content-Type"] = "application/xml"
    return response
