from os import environ

DEBUG = True
LOG_LEVEL = environ.get("LOG_LEVEL", default="DEBUG")  # CRITICAL / ERROR / WARNING / INFO / DEBUG
LOG_DIR = environ.get("LOG_DIR", default="/tmp")
APPLICATION_LOG = LOG_DIR + '/email-autoconf.log'

SENTRY_CONFIG = {
    'enabled': environ.get('SENTRY_ENABLED', False),
    'dsn': environ.get('SENTRY_DSN', None),
    'environment': environ.get('SENTRY_ENV', 'dev')
}

DB_TYPE = environ.get("DB_TYPE", default="none")
DB_HOST = environ.get("DB_HOST", default="127.0.0.1")
DB_PORT = environ.get("DB_PORT", default=3306)
DB_NAME = environ.get("DB_NAME", default="email-autoconf")
DB_USER = environ.get("DB_USER", default="root")
DB_PASSWORD = environ.get("DB_PASS", default="")

SELECT_USERNAME = environ.get("SELECT_USERNAME", default=None)
SELECT_DISPLAY_NAME = environ.get("SELECT_DISPLAY_NAME", default=None)

IMAP_SERVER_HOSTNAME = environ.get("IMAP_SERVER_HOSTNAME")
IMAP_SERVER_PORT = environ.get("IMAP_SERVER_PORT", default=993)
IMAP_SERVER_SSL = environ.get("IMAP_SERVER_SSL", default=True)

POP3_SERVER_HOSTNAME = environ.get("POP3_SERVER_HOSTNAME")
POP3_SERVER_PORT = environ.get("POP3_SERVER_PORT", default=995)
POP3_SERVER_SSL = environ.get("POP3_SERVER_SSL", default=True)

SMTP_SERVER_HOSTNAME = environ.get("SMTP_SERVER_HOSTNAME")
SMTP_SERVER_PORT = environ.get("SMTP_SERVER_PORT", 465)
SMTP_SERVER_SSL = environ.get("SMTP_SERVER_SSL", True)
SMTP_GLOBAL_PREFERRED = environ.get("SMTP_GLOBAL_PREFERRED", False)

EMAIL_PROVIDER = environ.get("EMAIL_PROVIDER", default="example.com")
