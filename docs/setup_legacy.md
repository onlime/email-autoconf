## (LEGACY) Setup Dev Environment

> This documentation is only used as legacy dev notes. For a production setup, please consult [documentation.md](documentation.md) instead!

Install and run email-autoconf in dev environment:

```bash
$ apt install python3
$ git clone git@gitlab.com:onlime/email-autoconf.git
$ cd email-autoconf
$ python3 -m venv venv
$ . venv/bin/activate
$ pip install -r requirements.txt
$ export FLASK_APP=server.py FLASK_DEBUG=1 FLASK_RUN_PORT=8090
$ flask run
```

Activating venv and firing up flask can be done with this helper script:

```bash
$ ./contrib/flask.sh
```

Access the app by running http://localhost:8090 in your browser.

## Initial Setup

The following describes initial setup upon project creation, starting from scratch.

### Python virtual environment

Python 3 comes bundled with the [`venv`](https://docs.python.org/3/library/venv.html#module-venv) module to create virtual environments:

```bash
$ mkdir email-config
$ cd email-config
$ python3 -m venv venv
```

Before you work on your project, activate the corresponding environment:

```bash
$ . venv/bin/activate

(venv) $ python --version
Python 3.6.5
```

Deactivate the virtual environment:

```bash
$ deactivate
```

### Install Flask

Within the activated environment, use the following command to install Flask:

```bash
$ pip install --upgrade pip
$ pip install Flask
Successfully installed Flask-1.0.1 Jinja2-2.10 MarkupSafe-1.0 Werkzeug-0.14.1 click-6.7 itsdangerous-0.24
```

### Prepare git repo

- [Best Practice for virtualenv and git repo](http://libzx.so/main/learning/2016/03/13/best-practice-for-virtualenv-and-git-repos.html)
- [Is it bad to have my virtualenv directory inside my git repository?](https://stackoverflow.com/a/6590783)

```bash
$ echo "/venv/" >> .gitignore
$ pip freeze > requirements.txt
$ git init
$ git add .
$ git commit -m "initial commit"
```

`requirements.txt` the looks like this:

```
click==6.7
Flask==1.0.1
itsdangerous==0.24
Jinja2==2.10
MarkupSafe==1.0
Werkzeug==0.14.1
```

Deployment would then go somehow like this:

```bash
$ git pull
$ python3 -m venv venv && . venv/bin/activate && pip install -r requirements.txt
```

### Upgrading pip packages

- [A `pip` hack to upgrade all your Python packages](https://hackernoon.com/a-pip-hack-to-upgrade-all-your-python-packages-492658c49681)
- [Upgrading all packages with pip](https://stackoverflow.com/a/3452888)

You may list your installed packages and outdated ones like this:

```bash
(venv)$ pip list
(venv)$ pip list --outdated
```

Use the following hack to upgrade all packages:

```bash
(venv)$ pip list --outdated --format=freeze | grep -v '^\-e' | cut -d = -f 1  | xargs -n1 pip install -U
```

then, you might want to merge `freeze` output into your `requirements.txt` (don't put all dependencies in there!):

```bash
(venv)$ pip freeze — local
```
