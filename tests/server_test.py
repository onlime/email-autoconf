import unittest
from email_autoconf import server

app = server.app


class ServerTests(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls._jwt = None

    @classmethod
    def tearDownClass(cls):
        pass

    def setUp(self):
        self.client = app.test_client()
        self.client.testing = True

    def tearDown(self):
        pass

    def test_index(self):
        result = self.client.open('/')
        self.assertRegex(result.data.decode(), 'configuring your mailaccount')
        self.assertEqual(result.status_code, 200)

    def test_autodiscover(self):
        result = self.client.open(
            '/autodiscover/autodiscover.xml',
            method='post',
            data='<?xml version="1.0" encoding="utf-8"?>\n<Autodiscover xmlns="http://schemas.microsoft.com/exchange/autodiscover/outlook/requestschema/2006">\n<Request>\n<AcceptableResponseSchema>http://schemas.microsoft.com/exchange/autodiscover/outlook/responseschema/2006a</AcceptableResponseSchema>\n<EMailAddress>testemail@example.com</EMailAddress>\n</Request>\n</Autodiscover>'  # noqa: E501
        )
        self.assertRegex(result.data.decode(), 'testemail@example.com')
        result = self.client.open(
            '/autodiscover/autodiscover.xml',
            method='post',
            data='<?xml version="1.0" encoding="utf-8"?>\n<Autodiscover xmlns="http://schemas.microsoft.com/exchange/autodiscover/outlook/requestschema/2006">\n<Request>\n<AcceptableResponseSchema>http://schemas.microsoft.com/exchange/autodiscover/outlook/responseschema/2006a</AcceptableResponseSchema>\n<EMailAddress>testemail-without-at</EMailAddress>\n</Request>\n</Autodiscover>'  # noqa: E501
        )
        self.assertEqual(result.status_code, 400)
        result = self.client.open('/autodiscover/autodiscover.xml', method='post')
        self.assertEqual(result.status_code, 400)
        self.assertRegex(result.data.decode(), 'request body could not be parsed as XML')
        result = self.client.open(
            '/autodiscover/autodiscover.xml',
            method='post',
            data='<?xml version="1.0" encoding="utf-8"?>\n<Autodiscover xmlns="http://schemas.microsoft.com/exchange/autodiscover/outlook/requestschema/2006">\n</Autodiscover>'  # noqa: E501
        )
        self.assertEqual(result.status_code, 400)
        self.assertRegex(result.data.decode(), 'Missing field in XML request body: \'Request\'')
        result = self.client.open(
            '/autodiscover/autodiscover.xml',
            method='post',
            data='<?xml version="1.0" encoding="utf-8"?>\n<Autodiscover xmlns="http://schemas.microsoft.com/exchange/autodiscover/outlook/requestschema/2006">\n<Request>\n<AcceptableResponseSchema>http://schemas.microsoft.com/exchange/autodiscover/outlook/responseschema/2006a</AcceptableResponseSchema>\n<EMailAddress></EMailAddress>\n</Request>\n</Autodiscover>'  # noqa: E501
        )
        self.assertEqual(result.status_code, 400)
        self.assertRegex(result.data.decode(), '`EMailAddress` field is empty.')

    def test_autoconfiguration(self):
        result = self.client.open('/mail/config-v1.1.xml?emailaddress=testaddress@example.com', method='get')
        self.assertRegex(result.data.decode(), 'testaddress@example.com')
        result = self.client.open('/mail/config-v1.1.xml?emailaddress=testaddress-without-at.example', method='get')
        self.assertEqual(result.status_code, 400)
        result = self.client.open('/mail/config-v1.1.xml', method='get')
        self.assertEqual(result.status_code, 200)
        self.assertRegex(result.data.decode(), '%EMAILADDRESS%')

    def test_mobileconf(self):
        result = self.client.open('/email.mobileconfig?email=testaddress@example.com', method='get')
        self.assertRegex(result.data.decode(), 'testaddress@example.com')
        result = self.client.open('/email.mobileconfig?email=testaddress-without-at.example', method='get')
        self.assertEqual(result.status_code, 400)
        self.assertRegex(result.data.decode(), 'No address provided')
        result = self.client.open('/email.mobileconfig', method='get')
        self.assertEqual(result.status_code, 400)
        self.assertRegex(result.data.decode(), 'No `email` parameter provided')
