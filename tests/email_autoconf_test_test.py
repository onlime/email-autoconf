import unittest
from scripts import email_autoconf_test
from unittest.mock import MagicMock


class EmailAutoconfTestTests(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        pass

    @classmethod
    def tearDownClass(cls):
        pass

    def setUp(self):
        self.client = email_autoconf_test.EmailAutoconfClient('testaddress@example.com')

    def tearDown(self):
        pass

    def test_find_servers(self):
        servers = self.client.find_servers('thunderbird')
        self.assertTrue('autoconfig.example.com' in servers)
        email_autoconf_test.check_output = MagicMock()
        servers = self.client.find_servers('microsoft')
        email_autoconf_test.check_output.assert_called_with(['dig', '+short', '-t', 'SRV', '_autodiscover._tcp.example.com'])
        self.assertTrue('autodiscover.example.com' in servers)
