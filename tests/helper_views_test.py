import unittest

from email_autoconf.helper.views import Views


class ViewsTests(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        pass

    @classmethod
    def tearDownClass(cls):
        pass

    def setUp(self):
        config = {
            "IMAP_SERVER_PORT": 993,
            "IMAP_SERVER_HOSTNAME": 'imap.example.com',
            "IMAP_SERVER_SSL": True,
            "POP3_SERVER_PORT": 995,
            "POP3_SERVER_HOSTNAME": 'pop.example.com',
            "POP3_SERVER_SSL": True,
            "SMTP_SERVER_PORT": 465,
            "SMTP_SERVER_HOSTNAME": 'smtp.example.com',
            "SMTP_SERVER_SSL": True,
            "EMAIL_PROVIDER": 'Testprovider'
        }
        self.views = Views(config)

    def test_render_view(self):
        # rendered = self.views.render_view("Autodiscover.xml.j2", 'testusername', 'testdisplayname', 'example.com')
        pass
