import unittest
from unittest.mock import MagicMock

from email_autoconf.helper import database


class DatabaseTests(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        pass

    @classmethod
    def tearDownClass(cls):
        pass

    def setUp(self):
        config = {
            "DB_TYPE": "none",
            "DB_HOST": "testhost",
            "DB_PORT": 3306,
            "DB_USER": "testuser",
            "DB_PASSWORD": "testpassword",
            "DB_NAME": "testdb",
            "SELECT_USERNAME": "SELECT username FROM usertable WHERE address = %a",
            "SELECT_DISPLAY_NAME": "SELECT displayname FROM usertable WHERE username = %u",
        }
        self.database = database.Database(config, MagicMock())

    def test_connect_mysql(self):
        database.pymysql = MagicMock()
        self.database.connect_mysql()
        database.pymysql.connect.assert_called_with(
            host="testhost",
            port=3306,
            user="testuser",
            passwd="testpassword",
            database="testdb",
        )
        self.database.db.cursor.assert_called_with()

    def test_get_username(self):
        self.database.connect_mysql()  # we need a real db connection for db.escape() to work
        self.database.cursor = MagicMock()

        self.database.get_username('testaddress@example.com')
        self.database.cursor.execute.assert_called_with(
            "SELECT username FROM usertable WHERE address = 'testaddress@example.com'"
        )
        # TODO: do real db lookups and assert username
        # self.assertEqual(username, 'testuser')

        # TODO: do another real db lookup for an email address with same username
        # username = self.database.get_username('testaddress3@example.com')
        # self.assertEqual(username, 'testaddress3@example.com')

    def test_get_displayname(self):
        self.database.connect_mysql()  # we need a real db connection for db.escape() to work
        self.database.cursor = MagicMock()

        self.database.db.escape = MagicMock(return_value="'testusername'")
        display_name = self.database.get_displayname('testaddress@example.com', 'testusername')
        self.database.cursor.execute.assert_called_with("SELECT displayname FROM usertable WHERE username = 'testusername'")

        self.database.config["SELECT_DISPLAY_NAME"] = None
        display_name = self.database.get_displayname('testaddress@example.com', 'testusername')
        self.assertEqual(display_name, 'testaddress@example.com')

        self.database.db.escape = MagicMock(return_value="'testaddress@example.com'")
        self.database.config["SELECT_DISPLAY_NAME"] = "SELECT displayname FROM usertable WHERE address = %a"
        display_name = self.database.get_displayname('testaddress@example.com', 'testusername')
        self.database.cursor.execute.assert_called_with(
            "SELECT displayname FROM usertable WHERE address = 'testaddress@example.com'"
        )
