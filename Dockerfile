FROM python:3.11-bookworm
# FROM --platform=linux/amd64 python:3.11-bookworm

RUN apt-get update \
    && apt-get install -y apt-utils \
    && apt-get -y dist-upgrade \
    && apt-get install -y netcat-openbsd lsb-release

## Install percona-server-client (instead of Debian's default-mysql-client)
## mysql_config_editor is part of libperconaserverclient20-dev package
RUN wget -O /usr/src/percona-release.deb https://repo.percona.com/apt/percona-release_latest.$(lsb_release -sc)_all.deb \
  && dpkg -i /usr/src/percona-release.deb \
  && percona-release setup ps80 \
  && apt-get update \
  && export DEBIAN_FRONTEND=noninteractive
RUN apt-get install -q -y percona-server-client libperconaserverclient21-dev

RUN touch /var/log/uwsgi-req.log /var/log/uwsgi-err.log

RUN adduser email-autoconf --home /app --system --disabled-password --group \
    && chown email-autoconf /app /var/log/uwsgi-req.log /var/log/uwsgi-err.log

WORKDIR /app

COPY requirements.txt ./

ENV PYTHONDONTWRITEBYTECODE=1

RUN pip install --upgrade pip
RUN pip install --upgrade -r requirements.txt
RUN pip install coverage

ADD . /app/

RUN /app/docker/build.sh

USER email-autoconf

EXPOSE 8080

# Only set these Flask env vars, if running `flask run --host=0.0.0.0` in docker/start.sh
# ENV FLASK_APP=server.py FLASK_DEBUG=1 FLASK_RUN_PORT=8090

CMD [ "/app/docker/start.sh" ]
