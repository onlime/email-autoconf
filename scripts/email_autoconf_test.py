#!/usr/bin/env python3

import argparse
import requests
from subprocess import check_output
from urllib3.exceptions import InsecureRequestWarning

requests.packages.urllib3.disable_warnings(category=InsecureRequestWarning)


class EmailAutoconfClient(object):
    """
    The EmailAutoconfClient represents a client to test email autoconf

    Args:
        address (str): The address to get the email autoconf information for
        server (str): (optional) The server to ask for the email autoconf information

    Attributes:
        address (str): Variable to store the address
        domain (str): Variable to store the domain of the address
        server (array): Variable to store the server names to check against
    """

    PROTOS = ['https', 'http']

    def __init__(self, address: str, server: str = None):
        self.address = address
        self.domain = address.split('@')[1]
        self.server = None if server is None else [server]

    def find_servers(self, type: str):
        """Retrieves the servers which should be checked

        Args:
            type (str): Find servers for `thunderbird` or `microsoft`

        Returns:
            array: List of servers to check
        """
        if type == 'thunderbird':
            return [
                'autoconfig.{}'.format(self.domain),
                '{}/.well-known/autoconfig'.format(self.domain),
            ]
        elif type == 'microsoft':
            servers = []
            srv_entry = check_output(['dig', '+short', '-t', 'SRV', '_autodiscover._tcp.{}'.format(self.domain)])
            try:
                servers.append(srv_entry.split()[3].decode('UTF-8')[:-1])
            except IndexError:
                pass

            servers.append('autodiscover.{}'.format(self.domain))
            servers.append(self.domain)
            return servers
        else:  # pragma: no cover
            raise NotImplementedError

    def do_thunderbird_lookup(self):  # pragma: no cover
        """Retrieves the Autoconfig for Thunderbird mail client
        """
        servers = self.find_servers('thunderbird') if self.server is None else self.server

        print('Testing Autoconfig')
        for server in servers:
            for proto in self.PROTOS:
                try:
                    url = '{}://{}/mail/config-v1.1.xml?emailaddress={}'.format(proto, server, self.address)
                    print('Try {}'.format(url))
                    response = requests.get(url, allow_redirects=False, verify=False)
                    if response.status_code == 200:
                        break
                    elif response.status_code == 301 or response.status_code == 302:
                        print('Got redirect ({}) to {}'.format(response.status_code, response.headers.get('location')))
                    else:
                        print('Response code: {}'.format(response.status_code))
                    print()
                except:  # noqa: E722
                    response = None
            if response is not None and response.status_code == 200:
                break

        if response is not None and response.status_code == 200:
            print('Autoconfig content:')
            print()
            print(response.text)
        else:
            print('No Autoconfig found for {}'.format(self.address))
        print()
        print('---')
        print()

    def do_microsoft_lookup(self):  # pragma: no cover
        """Retrieves the Autodiscover for Microsoft mail clients
        """
        servers = self.find_servers('microsoft') if self.server is None else self.server

        request = """<?xml version="1.0" encoding="utf-8"?>
<Autodiscover xmlns="http://schemas.microsoft.com/exchange/autodiscover/mobilesync/requestschema/2006">
    <Request>
        <AcceptableResponseSchema>http://schemas.microsoft.com/exchange/autodiscover/mobilesync/responseschema/2006</AcceptableResponseSchema>
        <EMailAddress>{}</EMailAddress>
    </Request>
</Autodiscover>""".format(self.address)

        print('Testing Autodiscover')
        for server in servers:
            for proto in self.PROTOS:
                try:
                    url = '{}://{}/autodiscover/autodiscover.xml'.format(proto, server)
                    print('Try {}'.format(url))
                    response = requests.post(
                        url, data=request, headers={'Content-Type': 'application/xml'}, allow_redirects=False, verify=False
                    )
                    if response.status_code == 200:
                        break
                    elif response.status_code == 301 or response.status_code == 302:
                        print('Got redirect ({}) to {}'.format(response.status_code, response.headers.get('location')))
                    else:
                        print('Response code: {}'.format(response.status_code))
                    print()
                except:  # noqa: E722
                    response = None
            if response is not None and response.status_code == 200:
                break

        if response is not None and response.status_code == 200:
            print('Autodiscover content:')
            print()
            print(response.text)
        else:
            print('No Autodiscover found for {}'.format(self.address))
        print()
        print('---')
        print()

    def do_mobile_lookup(self):  # pragma: no cover
        """Retrieves the Mobileconfig for iPhones
        """
        servers = self.find_servers('thunderbird') if self.server is None else self.server

        print('Testing Mobileconfig')
        for server in servers:
            for proto in self.PROTOS:
                try:
                    url = '{}://{}/email.mobileconfig?email={}'.format(proto, server, self.address)
                    print('Try {}'.format(url))
                    response = requests.get(url, allow_redirects=False, verify=False)
                    if response.status_code == 200:
                        break
                    elif response.status_code == 301 or response.status_code == 302:
                        print('Got redirect ({}) to {}'.format(response.status_code, response.headers.get('location')))
                    else:
                        print('Response code: {}'.format(response.status_code))
                    print()
                except:  # noqa: E722
                    response = None
            if response is not None and response.status_code == 200:
                break

        if response is not None and response.status_code == 200:
            print('Mobileconfig content:')
            print()
            print(response.text)
        else:
            print('No Mobileconfig found for {}'.format(self.address))
        print()
        print('---')
        print()


if __name__ == "__main__":  # pragma: no cover
    parser = argparse.ArgumentParser()
    parser.add_argument("address", help='Mailaddress of which you like to test the autoconfiguration')
    parser.add_argument("--server", help='Ask this server instead of the default ones', default=None)
    parser.add_argument("--no-thunderbird", help='Do not make the lookup for thunderbird autoconfig', default=False)
    parser.add_argument("--no-microsoft", help="Do not make the lookup for microsoft autodiscovery", default=False)
    parser.add_argument("--no-mobileconfig", help="Do not make the lookup for mobileconfig", default=False)
    args = parser.parse_args()

    client = EmailAutoconfClient(args.address, args.server)

    if not args.no_thunderbird:
        client.do_thunderbird_lookup()
    if not args.no_microsoft:
        client.do_microsoft_lookup()
    if not args.no_mobileconfig:
        client.do_mobile_lookup()
