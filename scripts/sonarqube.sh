#!/bin/bash

echo $SONAR_HOST

sonar-scanner \
    -Dsonar.login="$SONAR_LOGIN" \
    -Dsonar.projectKey=email-autoconf \
    -Dsonar.projectName="Email Autoconf" \
    -Dsonar.host.url="$SONAR_HOST" \
    -Dsonar.projectBaseDir=/root \
    -Dsonar.sources=./src/email_autoconf \
    -Dsonar.test.inclusions=**/*_test.py \
    -Dsonar.exclusions=**/venv/** \
    -Dsonar.coverage.exclusions=src/openapi.py,src/wsgi.py,src/venv \
    -Dsonar.python.coverage.reportPaths=/root/src/coverage.xml