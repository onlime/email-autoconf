# email-autoconf: Simple mail client configuration

email-autoconf is a Python package to provide a simple but feature-rich way to automatically configure your customers email client. It unites methods for automated mailbox configuration from Apple's [Mobileconfig](https://support.apple.com/de-de/guide/profile-manager/pmdbd71ebc9/mac), Microsoft's [Autodiscover](https://docs.microsoft.com/en-us/exchange/architecture/client-access/autodiscover) and Mozilla's [Autoconfig](https://wiki.mozilla.org/Thunderbird:Autoconfiguration) in one tool.

email-autoconf supports two work modes. One is with a database in which it can lookup usernames and display names and the other is without a database.

## QUICKSTART

> **See [Documentation](docs/documentation.md) for more information!**

1. Clone the repository `git clone https://gitlab.com/onlime/email-autoconf.git` 
2. Create the venv with `python3 -m venv venv`
3. Activate the venv with `. venv/bin/activate`
4. Install required Python modules: `pip install -r requirements.txt`
5. Copy the example config from `instance/settings.py_example` to `instance/settings.py` and edit it to match your needs:
    1. Enter DB settings if needed otherwise comment it out.
    2. Adjust the SQL query or comment it out if one or both aren't needed.
    3. Change the server names to reflect your mail system
    4. Change the provider setting to whatever you need.
6. Run the instance with `uwsgi --yaml uwsgi-http.yml --yaml uwsgi-http-instance.yml`
7. Try it with `curl localhost:8080/mail/config-v1.1.xml?emailaddress=testaddress@example.com`

We recommend to use an Apache or Nginx in front of this uWSGI server for example to provide HTTPS support.

## Documentation

Detailed documentation is available here: **[docs/documentation.md](docs/documentation.md)**

## Automx2 vs. email-autoconf

[email-autoconf](https://gitlab.com/onlime/email-autoconf) is meant to be a simpler and more flexible alternative to [automx2](https://automx.org) (GitHub: [rseichter/automx2](https://github.com/rseichter/automx2)). Back in Feb 2020, when we started with this project, automx2 was still in its early stages and did not allow us to easily set up a database backend for mailaccount username lookups.

But in the meantime, automx2 has greatly evolved and might be the better option for you. It's your choice.

## About email-autoconf

The project is hosted on [GitLab](https://gitlab.com/onlime/email-autoconf).
Please use the project's [issue tracker](https://gitlab.com/onlime/email-autoconf/issues) if you find bugs.

email-autoconf was written by [Philip Iezzi @piezzi](https://gitlab.com/piezzi) and [Martin Wittwer @wittwer-it](https://gitlab.com/wittwer-it) for [Onlime GmbH](https://www.onlime.ch).