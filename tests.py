#!/usr/bin/env python3
from unittest import defaultTestLoader, TextTestResult, TextTestRunner

if __name__ == '__main__':
    test_suite = defaultTestLoader.discover('tests', '*_test.py')
    test_runner = TextTestRunner(resultclass=TextTestResult)
    result = test_runner.run(test_suite)
    # We can not use sys.exit() here because it does not work after `defaultTestLoader`
    raise SystemExit(not result.wasSuccessful())
