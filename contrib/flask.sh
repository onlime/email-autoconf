#!/usr/bin/env bash
# vim:tabstop=4:noexpandtab
#
# Launches email-autoconf as a Flask application. Execute this script from
# within the parent directory of your Python venv. Example usage:
#
# (1) flask.sh run --port=4243
# Launches application for http://127.0.0.1:4243/ . This is the typical
# production configuration when running behind a proxy server.
#
# (2) flask.sh run --host=somehost.example.com --port=80
# Launches application for http://somehost.example.com/ . This allows
# email-autoconf to run without a proxy server.

set -e
. venv/bin/activate

# User configurable section -- START

# Set the following to either 'development' or 'production'.
export FLASK_ENV='production'

# Debug mode can be controlled separately from the environment
#export FLASK_DEBUG=1

# Set default port, can be overridden by --port=XXXX
export FLASK_RUN_PORT=8090

# User configurable section -- END

export FLASK_APP='email_autoconf.server:app'
flask "${@:-run}"
