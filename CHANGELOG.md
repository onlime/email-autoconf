# CHANGELOG

## [2023.x (Unreleased)](https://gitlab.com/onlime/email-autoconf/compare/2023.11...main)

## [2023.11 (2023-11-28)](https://gitlab.com/onlime/email-autoconf/compare/2023.10...2023.11)

- Added `cryptography` requirement (fixes `RuntimeError: 'cryptography' package is required for sha256_password or caching_sha2_password auth methods`)

## [2023.10 (2023-10-25)](https://gitlab.com/onlime/email-autoconf/compare/2021.8...2023.10)

- Upgraded to Flask 3.0
- Minimum Python version is now 3.9
- Updated testing / Docker image to `python:3.11-bookworm`
- Replaced MySQL Connector/Python (mysql-connector-python) by [PyMySQL](https://pypi.org/project/pymysql/)
- Improved PEP8 Python code style: Added configuration for [YAPF](https://github.com/google/yapf) and applied yapf formatting rules, linting with [Flake8](https://flake8.pycqa.org/)
- Added HOWTO for local testing to `docs/documentation.md`

## [2021.8 (2021-08-25)](https://gitlab.com/onlime/email-autoconf/-/tags/2021.8)

- First public release
